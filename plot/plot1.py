# -*- coding: utf-8 -*-
"""
Created on Fri Dec 30 14:23:08 2011

@author: atm
"""

import csv
import pylab

plot_data = open("plot_data.csv","r")
read_data = csv.reader(plot_data)
read_data.next()

sale = []
qty = []
for row in read_data:
    sale.append(row[0])
    qty.append(row[3])
    
pylab.plot(sale, qty, 'g')
pylab.savefig('sale_data.png')