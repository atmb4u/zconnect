from disco.core import Job, result_iterator
import pylab as p
def map(line, params):
#    for no, date, customer, purchased, price, offer, festive, branch  in line.split(','):
#        yield customer, int(purchased)
#    for word in line.split():
    try:
        no, date, customer, purchased, price, offer, festive, branch = line.split(',')
        yield branch, 1
    except:
        yield "Failed", 1

def reduce(iter, params):
    from disco.util import kvgroup
    for word, counts in kvgroup(sorted(iter)):
        yield word, sum(counts)

if __name__ == '__main__':
    job = Job().run(input=["/home/atm/profoundis/mapreduce/Analytics Data1 - Sheet 1.csv"],
                    map=map,
                    reduce=reduce)
    counter = 0
    lowcount = 100000000000
    word_arr = []
    count_arr = []
    for word, count in result_iterator(job.wait(show=True)):
        if count > counter:
            wording = word
            counter = count
        try:
            test = int(word)            
            if count < lowcount and word != "Failed":
                wordout = word
                lowcount = count
        except:
            pass
        if word != "Failed":
            try:
                word_arr.append(int(word))
                count_arr.append(int(count))
                print word, int(count)
            except:
                pass
    print "Highest count:", wording, counter
    print "Lowest count:", wordout, lowcount
    fig = p.figure()
    ax = fig.add_subplot(1,1,1)
    ax.bar(word_arr,count_arr, facecolor='#00AA00', width=0.5, align='center' )
    ax.set_title('Sale, by Outlet Location',fontstyle='italic')
    ax.set_xticks([1,2,3,4])
    group_labels = ['Kottayam', 'Ernakulam',
                    'Thiruvananthapuram', 'Kozhikode']
    ax.set_xticklabels(group_labels)
    fig.autofmt_xdate()
    #pylab.bar(word_arr,count_arr, facecolor='#00AA00', width=0.1, align='center')
    #pylab.set_xlabel("Number of Sales")
    p.savefig("branchsale.png")
#        print word, count
