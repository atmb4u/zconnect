from disco.core import Job, result_iterator
import pylab as p
import csv
def map(line, params):
#    for no, date, customer, purchased, price, offer, festive, branch  in line.split(','):
#        yield customer, int(purchased)
#    for word in line.split():
    try:
        no, date, customer, purchased, price, offer, festive, branch = line.split(',')
        yield customer, 1
    except:
        yield "Failed", 1

def reduce(iter, params):
    from disco.util import kvgroup
    for word, counts in kvgroup(sorted(iter)):
        yield word, sum(counts)

if __name__ == '__main__':
    job = Job().run(input=["/home/atm/Downloads/Analytics Data1 - Sheet 1.csv"],
                    map=map,
                    reduce=reduce)
    counter = 0
    lowcount = 100000000000
    word_arr = []
    count_arr = []
    num = 0
    for word, count in result_iterator(job.wait(show=True)):
        num+=1
        if count > counter:
            wording = word
            counter = count
        if count < lowcount and word != "Failed":
            wordout = word
            lowcount = count
        if word !="Failed":
            try:
                word_arr.append(num)
                count_arr.append(int(count))
            except:
                pass
            print word, count
            
    print "Highest count:", wording, counter
    print "Lowest count:", wordout, lowcount
    plot_data = open("/home/atm/Downloads/Analytics Data1 - Sheet 1.csv","r")
    read_data = csv.reader(plot_data)
    read_data.next()
    customer = []
    for row in read_data:
        customer.append(row[2])
    customer.sort()
#        print word, count
    fig = p.figure()
    ax = fig.add_subplot(1,1,1)
    ax.bar(word_arr,count_arr, facecolor='#00AA00', width=0.1, align='center')
    #ax.plot(word_arr,count_arr,'g')
    ax.set_title('Sale, by Date',fontstyle='italic')
    ax.set_xticks([x for x in range(0,213)])
    #group_labels = ['Kottayam', 'Ernakulam',
    #                'Thiruvananthapuram', 'Kozhikode']
    ax.set_xticklabels(customer)
    #fig.autofmt_xdate()
    #pylab.bar(word_arr,count_arr, facecolor='#00AA00', width=0.1, align='center')
    #pylab.set_xlabel("Number of Sales")
    p.show()
    #p.savefig("customersale.png")