from disco.core import Job, result_iterator
import pylab as p
def map(line, params):
#    for no, date, customer, purchased, price, offer, festive, branch  in line.split(','):
#        yield customer, int(purchased)
#    for word in line.split():
    try:
        no, date, customer, purchased, price, offer, festive, branch = line.split(',')
        yield int(price), 1
    except:
        yield "Failed", 1

def reduce(iter, params):
    from disco.util import kvgroup
    for word, counts in kvgroup(sorted(iter)):
        yield word, sum(counts)

if __name__ == '__main__':
    job = Job().run(input=["/home/atm/Downloads/Analytics Data1 - Sheet 1.csv"],
                    map=map,
                    reduce=reduce)
    counter = 0
    lowcount = 100000000000
    lowprice = 1000000000000
    highprice = 0
    word_arr = []
    count_arr = []
    for word, count in result_iterator(job.wait(show=True)):
        if count > counter:
            wording = word
            counter = count
        if count < lowcount and word != "Failed":
            wordout = word
            lowcount = count
        if word != 'Failed':
            if lowprice > int(word):
                lowprice = int(word)
            if highprice < int(word):
                highprice = int(word)
            word_arr.append(int(word))
            count_arr.append(int(count))
    print "Highest count:", wording, counter
    print "Lowest count :", wordout, lowcount
    print "Lowest Price :", lowprice
    print "Highest Price :", highprice
#        print word, count
    fig = p.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(word_arr,count_arr, 'r' )
    ax.set_title('Sale, by Price',fontstyle='italic')
    #ax.set_xticks([1,2,3,4])
#    group_labels = ['Kottayam', 'Ernakulam',
#                    'Thiruvananthapuram', 'Kozhikode']
#    ax.set_xticklabels(group_labels)
    fig.autofmt_xdate()
    #pylab.bar(word_arr,count_arr, facecolor='#00AA00', width=0.1, align='center')
    #pylab.set_xlabel("Number of Sales")
    p.savefig("price.png")