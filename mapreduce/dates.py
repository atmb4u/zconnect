from disco.core import Job, result_iterator
import pylab as p
def map(line, params):
#    for no, date, customer, purchased, price, offer, festive, branch  in line.split(','):
#        yield customer, int(purchased)
#    for word in line.split():
    try:
        no, date, customer, purchased, price, offer, festive, branch = line.split(',')
        yield date, 1
    except:
        yield "Failed", 1

def reduce(iter, params):
    from disco.util import kvgroup
    for word, counts in kvgroup(sorted(iter)):
        if word =="Failed":
            yield None, None
        else:
            yield word, sum(counts)

if __name__ == '__main__':
    job = Job().run(input=["/home/atm/Downloads/Analytics Data1 - Sheet 1.csv"],
                    map=map,
                    reduce=reduce)
    counter = 0
    lowcount = 100000000000
    word_arr = []
    count_arr = []
    num = 0
    for word, count in result_iterator(job.wait(show=True)):
        num+=1
        if count > counter:
            wording = word
            counter = count
        if count < lowcount and word != "Failed":
            wordout = word
            lowcount = count
        if word != "Failed":
            try:
                word_arr.append(num)
                count_arr.append(int(count))
            except:
                pass
            print word, int(count)
    print "Highest count:", wording, counter
    print "Lowest count:", wordout, lowcount
#        print word, count
    fig = p.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(word_arr,count_arr,'g')
    ax.set_title('Sale, by Date',fontstyle='italic')
    ax.set_xticks([x for x in range(0,70,5)])
    #group_labels = ['Kottayam', 'Ernakulam',
    #                'Thiruvananthapuram', 'Kozhikode']
    ax.set_xticklabels([x for x in range(0,70,5)])
    #fig.autofmt_xdate()
    #pylab.bar(word_arr,count_arr, facecolor='#00AA00', width=0.1, align='center')
    #pylab.set_xlabel("Number of Sales")
    p.savefig("datesale.png")
#        print word, count