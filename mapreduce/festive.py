from disco.core import Job, result_iterator
def map(line, params):
#    for no, date, customer, purchased, price, offer, festive, branch  in line.split(','):
#        yield customer, int(purchased)
#    for word in line.split():
    try:
        no, date, customer, purchased, price, offer, festive, branch = line.split(',')
        if festive == 'Y':
            yield customer, 1
        else:
            yield customer, -1
    except:
        yield "Failed", 1

def reduce(iter, params):
    from disco.util import kvgroup
    for word, counts in kvgroup(sorted(iter)):
        if word =="Failed":
            yield None, None
        else:
            yield word, sum(counts)

if __name__ == '__main__':
    job = Job().run(input=["/home/atm/Downloads/Analytics Data1 - Sheet 1.csv"],
                    map=map,
                    reduce=reduce)
    counter = 0
    lowcount = 100000000000
    for word, count in result_iterator(job.wait(show=True)):
        if count > counter:
            wording = word
            counter = count
        if count < lowcount and word != "Failed":
            wordout = word
            lowcount = count
        if word != "Failed":
            print word, int(count)
    print "Highest count:", wording, counter
    print "Lowest count:", wordout, lowcount
#        print word, count
