from disco.core import Job, result_iterator
def map(line, params):
    try:
        no, date, customer, purchased, price, offer, festive, branch = line.split(',')
        yield customer, int(price)*int(purchased)
    except:
        yield "Failed", 1

def reduce(iter, params):
    from disco.util import kvgroup
    for word, counts in kvgroup(sorted(iter)):
        yield word, sum(counts)

if __name__ == '__main__':
    job = Job().run(input=["/home/atm/Downloads/Analytics Data1 - Sheet 1.csv"],
                    map=map,
                    reduce=reduce)
    counter = 0
    lowcount = 100000000000
    for word, count in result_iterator(job.wait(show=True)):
        if count > counter:
            wording = word
            counter = count
        if count < lowcount and word != "Failed":
            wordout = word
            lowcount = count
        if word != "Failed":
            print word, count
    print "Highest Amount: Client %s Rs.%s" % (wording, counter)
    print "Lowest  Amount: CLient %s Rs.%s" % (wordout, lowcount)
#        print word, count
