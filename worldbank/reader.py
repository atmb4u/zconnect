import csv
import logging
import pylab
f = open('data/databank.csv', "rb")
rox = []
country_list = []
num = 0
for count, content in enumerate(csv.DictReader(f)):
    rox.append(content)
for row in rox:
    if row['Country Name'] not in country_list:
        country_list.append(row['Country Name'])
#print "Country List %s" % (country_list)
logging.basicConfig(filename='warning.log', level=logging.INFO)
for country in country_list:
    for row in rox:
        if row['Country Name'] == country:
            if row['Indicator Name'] == 'GDP (current US$)':
                years = []
                GDP = []
                pylab.clf()
                pylab.cla()
                pylab.close()
                for year in range(1963, 2010):
                    try:
                        GDP.append(float(row[str(year)]))
                        years.append(year)
                    except:
                        logging.info("No info on %s about %s in %s" %  \
                        (row['Country Name'], row['Indicator Name'], year))
                from matplotlib import pyplot
                fig = pyplot.figure()
                ax = fig.add_subplot(111)
                ax.plot(years, GDP, 'g')
                #ax.legend(legendStrings, loc = 'best')
                fig.savefig('GDP_1963_2010_%s.png' % (row['Country Name']))
                print 'GDP_1963_2010_%s.png saved.' % (row['Country Name'])

                #import ipdb; ipdb.set_trace();
